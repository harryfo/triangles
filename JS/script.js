/* Defined Shuffle Function */

function shuffle(a) {
    for (var i = a.length; i > 0; i--) {
        var j = Math.floor(Math.random() * i);
        var temp = a[i - 1];
        a[i - 1] = a[j];
        a[j] = temp;
    }
    return a;
}

/* Up Triangles Big */

var chooseFrom = ['criminal1', 'osiris1', 'hype2', 'oiler1', 'adidas1', 'diesel1', 'v1', 'evisu1'];

$(function() {
      shuffle(chooseFrom);
      for (var i = 0; i < chooseFrom.length; i++) 
      {          
        $('.UpTriBig' + (i+1)).attr('fill', 'url(#' + (chooseFrom[i]) + ')');
        $('.UpTriBig' + (i+1)).attr('onclick', "document.getElementById('" + (chooseFrom[i]) + "_content').style.display='block';document.getElementById('fade').style.display='block'");
      }
});

/* Down Triangles Big */

var chooseFrom2 = ['hype3', 'hollister1', 'hollister2', 'abercrombie1', 'jeremy1', 'hype1', 'yellow1'];

$(function() {
      shuffle(chooseFrom2);
      for (var i = 0; i < chooseFrom2.length; i++) 
      {          
        $('.DwnTriBig' + (i+1)).attr('fill', 'url(#' + (chooseFrom2[i]) + ')');
        $('.DwnTriBig' + (i+1)).attr('onclick', "document.getElementById('" + (chooseFrom2[i]) + "_content').style.display='block';document.getElementById('fade').style.display='block'");
      }
});

/* Up Triangles Small */

var chooseFrom3 = ['adidas2', 'jeremy2', 'helmet1', 'oiler2', 'trash1', 'viv1'];

$(function() {
      shuffle(chooseFrom3);
      for (var i = 0; i < chooseFrom3.length; i++) 
      {          
        $('.UpTriSm' + (i+1)).attr('fill', 'url(#' + (chooseFrom3[i]) + ')');
        $('.UpTriSm' + (i+1)).attr('onclick', "document.getElementById('" + (chooseFrom3[i]) + "_content').style.display='block';document.getElementById('fade').style.display='block'");
      }
});

/* Down Triangles Small */

var chooseFrom4 = ['wallet1', 'trash2', 'trash3', 'swatch1', 'swatch2', 'storm1', 'oiler3', 'oiler4', 'jeremy3', 'asos1'];

$(function() {
      shuffle(chooseFrom4);
      for (var i = 0; i < chooseFrom4.length; i++) 
      {          
        $('.DwnTriSm' + (i+1)).attr('fill', 'url(#' + (chooseFrom4[i]) + ')');
        $('.DwnTriSm' + (i+1)).attr('onclick', "document.getElementById('" + (chooseFrom4[i]) + "_content').style.display='block';document.getElementById('fade').style.display='block'");
          
          console.log(  '.DwnTriSm' + (i+1) );
      }
});

/* Display None Defs */

$(function() {
      for (var i = 0; i < chooseFrom4.length; i++) 
      {          
          $('#' + chooseFrom4[i] + "_content").css("display", "none");
          console.log('#' + chooseFrom4[i] + "_content");
      }
    
      for (var i = 0; i < chooseFrom2.length; i++) 
      {          
          $('#' + chooseFrom2[i] + "_content").css("display", "none");
          console.log('#' + chooseFrom2[i] + "_content");
      }
    
      for (var i = 0; i < chooseFrom.length; i++) 
      {          
          $('#' + chooseFrom[i] + "_content").css("display", "none");
          console.log('#' + chooseFrom[i] + "_content");
      }
      for (var i = 0; i < chooseFrom3.length; i++) 
      {          
          $('#' + chooseFrom3[i] + "_content").css("display", "none");
          console.log('#' + chooseFrom3[i] + "_content");
      }
    
});

var chooseFrom5 = [ '#95aad6' , '#c38ad1' , '#f0d187' , '#8ad195' ];
                   
$(function() {
    shuffle(chooseFrom5);
    for (var i = 0; i < chooseFrom5.length; i++) 
    {
        $(".background" + (i+1)) .css("background-color", chooseFrom5[i]);
}
});
